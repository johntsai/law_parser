#!/usr/bin/perl -w
# Get the counted number of occurance of keyword in johntsai.law (in berkerley_aligner_law.pl)

#require '/home/silentvow/public_html/cgi-bin/collocation/work17-lib_final.pl';
use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;

use lib '/home/denehs/public_html/cgi-bin/modules/';
use lib '/home/johntsai/pm';
#use getCut;
use MyParse;

# GLOBAL VARS

sub get_res
{
	#my @lines = @_;
	my $final = "";

	chdir("/home/johntsai/public_html/alignment/output");
	#my $langid = $param{"lang"};
	open RES_E, "<1.params.txt";
	open RES_F, "<2.params.txt";

	print STDERR "<b>Word alignment result </b><br>\n";

	# save all results
	while($_ = <RES_E>)
	{
		$final .= $_;
	}
	while($_ = <RES_F>)
	{
		$final .= $_;
	}
	close RES_E;
	close RES_F;

	#my $var3 = $dbh->quote($final);
	#$dbh->prepare("insert into Aligner (keyword, tag, content) values (?, ?, ?)")->execute($keyword,$tag,$final) if($final =~ m|\w|);

	#print "<hr>\n CTK Alignment Result:<br>\n".$final,"<hr>\n";
	#print "<hr>\n CTK Alignment Result:<br>\n".$var3,"<hr>\n";

	#print STDERR "Aligner return: ",$final,"\n";
	return $final;
}

# ===== start

#my $input_file;
my $sth;
my (%en_count,%ch_count);
my @lines;
my ($source,$q);

if(@ARGV>1){
	my $sth;
	$source = shift;
	$q = shift;
	$sth = $dbh->prepare("SELECT `id`, `en`, `ch_parsed` FROM `law` WHERE source LIKE ?");
	$sth->execute($source);
	while(my ($id,$en,$ch)=$sth->fetchrow_array()){
		print STDERR "$id:\n$en\n$ch\n=\n";

		if($en ne "" and $ch ne ""){
			$en =~ s|\r||g;
			$en =~ s|\n||g;
			$ch =~ s|\n||g;
			$ch =~ s|\r||g;
			# push results into @lines array
			my $tmp;
			$tmp->{string1} = $en;	# english string 
			$tmp->{string2} = $ch;	# ch
			push @lines, $tmp;

			# Calculate count of all words
			my @split = split /[^a-zA-Z]/,$en;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($en_count{$word} eq ""){
					$en_count{$word} = int(1);
				}else{
					$en_count{$word} += 1;
				}
			}

			utf8::decode($ch);
			@split = split /\s/,$ch;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($ch_count{$word} eq ""){
					$ch_count{$word} = int(1);
				}else{
					$ch_count{$word} += 1;
				}
			}
		}
	}
	print STDERR "\n===\n";
	print STDERR "Keyword: $q\n";
	print STDERR "Count: ";
=head
	if($ch_count{$q} ne ""){
		print $ch_count{$q};
	}else{
		print $en_count{$q};
	}
=cut
	print "$ch_count{$q}$en_count{$q}";
}else{
	die "Usage: perl get_count.pl <source_name> <keyword>\n";
}

=head
# Call Berkeley Word Aligner
$ret = get_res();
#print $ret,"\n";

# Find out results words w. tf >= 5
my @ret_lines = split /[\n\r]+/,$ret;

my $add =$dbh->prepare("INSERT INTO `BWA_law_parsed`( `source`, `keyword`, `content`) VALUES (?,?,?)");
for(my $i=0; $i<=$#ret_lines ; $i++){
	my $buffer;
	my $line = $ret_lines[$i];
	# Find the result
	if($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
		#$buffer = "$line\n";
		$buffer = "";
		$line =~ m|^\s*(\S+)|;
		my $key = $1;
		#last if($key =~ m|[0-9]| );
		if($en_count{$key}>=5 or $ch_count{$key}>=5){
			print $en_count{$key},"\n$ch_count{$key}\n\n";
			$i += 1;
			$line = $ret_lines[$i];
			until($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
				last if($line =~ m/0.000000/ eq "1");
				$buffer .= "$line\n";
				$i += 1;
				$line = $ret_lines[$i];
			}
			print "$source, $key($en_count{$key}/$ch_count{$key})-\t$buffer\n\n";
			#chomp(my $trash=<STDIN>) if($ch_count{$key}>=5);
			$add->execute($source,$key,$buffer);
		}
	}
}
=cut
print STDERR "done.\n";
