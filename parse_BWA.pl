#!/usr/bin/perl -w
# It's a LAZY program that doen not really run the BWA prorgram. It just read the result from already-run BWA and parse them, save the results of those words with tf>=5 into DB. (That is, the second half of berkerley_aligner_law.pl.)

use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use HTML::Entities;

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;

use lib '/home/denehs/public_html/cgi-bin/modules/';
use ZH::Ckipsvr;
use WordNet::QueryData;
use lib '/home/johntsai/pm';
#use getCut;
use MyParse;

# GLOBAL VARS

sub isstop_en
{
	my ($word) = @_;
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-en.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}

	close STOP;
	return $word;
}
sub tolemma
{
	my ($word, $tag, $wn) = @_;

	my @ary = $wn->validForms ("$word#$tag");

	if (scalar(@ary)==0)
	{
		return "?";
	}

	my $min = 100;
	my $ans;

	foreach my $i (@ary)
	{
		if (length($i)<=$min)
		{
			$min = length($i);
			$ans = $i;
		}
	}

	$ans =~ /(.*?)#.*/;
	$ans = $1;

	return $ans;
}
sub get_res
{
	#my @lines = @_;
	my $final = "";

	chdir("/home/johntsai/public_html/alignment/output");
	#my $langid = $param{"lang"};
	open RES_E, "<1.params.txt";
	open RES_F, "<2.params.txt";

	print "<b>Word alignment result </b><br>\n";

	# save all results
	while($_ = <RES_E>)
	{
		$final .= $_;
	}
	while($_ = <RES_F>)
	{
		$final .= $_;
	}
	close RES_E;
	close RES_F;

	#my $var3 = $dbh->quote($final);
	#$dbh->prepare("insert into Aligner (keyword, tag, content) values (?, ?, ?)")->execute($keyword,$tag,$final) if($final =~ m|\w|);

	#print "<hr>\n CTK Alignment Result:<br>\n".$final,"<hr>\n";
	#print "<hr>\n CTK Alignment Result:<br>\n".$var3,"<hr>\n";

	#print STDERR "Aligner return: ",$final,"\n";
	return $final;
}

# ===== start

#my $input_file;
#my $source='civil_code';
my $source;
my $sth;
my (%en_count,%ch_count);
my @lines;

if(@ARGV>0){
	my $sth;
	$source = shift;
	$sth = $dbh->prepare("SELECT `id`, `en`, `ch_parsed` FROM `law` WHERE source LIKE ?");
	$sth->execute($source);
	while(my ($id,$en,$ch)=$sth->fetchrow_array()){
		print "$id:\n$en\n$ch\n=\n";

		if($en ne "" and $ch ne ""){
			$en =~ s|\r||g;
			$en =~ s|\n||g;
			$ch =~ s|\n||g;
			$ch =~ s|\r||g;
			# push results into @lines array
			my $tmp;
			$tmp->{string1} = $en;	# english string 
			$tmp->{string2} = $ch;	# ch
			push @lines, $tmp;

			# Calculate count of all words
			my @split = split /[^a-zA-Z]/,$en;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($en_count{$word} eq ""){
					$en_count{$word} = int(1);
				}else{
					$en_count{$word} += 1;
				}
			}

			utf8::decode($ch);
			@split = split /\s/,$ch;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($ch_count{$word} eq ""){
					$ch_count{$word} = int(1);
				}else{
					$ch_count{$word} += 1;
				}
			}
		}
	}
}else{
	die "Usage: perl berkeley_aligner_law.pl <source_name>\n";
}

# Call Berkeley Word Aligner
$ret = get_res();
#print $ret,"\n";

# Find out results words w. tf >= 5
my @ret_lines = split /[\n\r]+/,$ret;
=head
foreach my $line(@ret_lines){
	if($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1" && $line !~ m|^\s*[a-z0-9]|i){
		print "$line\n";
		chomp(my$trash=<STDIN>);
	}
}
foreach my $key(keys %ch_count){
	print "$key:\t$ch_count{$key}\n";
}
chomp(my$trash=<STDIN>);
=cut

my $add =$dbh->prepare("INSERT INTO `BWA_law_parsed`( `source`, `keyword`, `content`) VALUES (?,?,?)");
for(my $i=0; $i<=$#ret_lines ; $i++){
	my $buffer;
	my $line = $ret_lines[$i];
	# Find the result
	if($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
		#$buffer = "$line\n";
		$buffer = "";
		$line =~ m|^\s*(\S+)|;
		my $key = $1;
		#last if($key =~ m|[0-9]| );
		if($en_count{$key}>=5 or $ch_count{$key}>=5){
			print $en_count{$key},"\n$ch_count{$key}\n\n";
			$i += 1;
			$line = $ret_lines[$i];
			until($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
				last if($line =~ m/0.000000/ eq "1");
				$buffer .= "$line\n";
				$i += 1;
				$line = $ret_lines[$i];
			}
			print "$source, $key($en_count{$key}/$ch_count{$key})-\t$buffer\n\n";
			#chomp(my $trash=<STDIN>) if($ch_count{$key}>=5);
			$add->execute($source,$key,$buffer);
		}
	}
}
print STDERR "done.\n";
