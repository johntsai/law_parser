#!/usr/bin/perl -w
# It's a EVEN LAZIER program that doen not really run the BWA prorgram. It just read the result from johntsai.BWA_law and parse them, save the results of those words with tf>=5 into DB. (That is, the second half of berkerley_aligner_law.pl.)

use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use HTML::Entities;

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;

use lib '/home/denehs/public_html/cgi-bin/modules/';
use ZH::Ckipsvr;
use WordNet::QueryData;
use lib '/home/johntsai/pm';
#use getCut;
use MyParse;

# GLOBAL VARS

sub isstop_en
{
	my ($word) = @_;
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-en.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}

	close STOP;
	return $word;
}
sub tolemma
{
	my ($word, $tag, $wn) = @_;

	my @ary = $wn->validForms ("$word#$tag");

	if (scalar(@ary)==0)
	{
		return "?";
	}

	my $min = 100;
	my $ans;

	foreach my $i (@ary)
	{
		if (length($i)<=$min)
		{
			$min = length($i);
			$ans = $i;
		}
	}

	$ans =~ /(.*?)#.*/;
	$ans = $1;

	return $ans;
}
sub get_res2
{
	my $source = shift;
	my $cmd = "SELECT `content` FROM `BWA_law` WHERE source LIKE ?";
	#print "$cmd : $source\n";
	my $sth = $dbh->prepare($cmd);
	$sth->execute($source);
	if(my ($final)= $sth->fetchrow_array()){
		utf8::decode($final);
		return $final;
	}else{
		return "not found.\n";
	}
}

# ===== start

#my $input_file;
#my $source='civil_code';
my $source;
my $sth;
my (%en_count,%ch_count);
my @lines;

if(@ARGV>0){
	my $sth;
	$source = shift;
}else{
	die "Usage: perl berkeley_aligner_law.pl <source_name>\n";
}

# Call Berkeley Word Aligner
$ret = get_res2($source);
print $ret,"\n";

#my $trash=<STDIN>;

my @ret_lines = split /[\n\r]+/,$ret;

my $add =$dbh->prepare("INSERT INTO `BWA_law_parsed`( `source`, `keyword`, `content`) VALUES (?,?,?)");
for(my $i=0; $i<=$#ret_lines ; $i++){
	my $buffer;
	my $line = $ret_lines[$i];
	# Find the result
	if($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
		#$buffer = "$line\n";
		$buffer = "";
		$line =~ m|^\s*(\S+)|;
		my $key = $1;
		utf8::decode($key);
		#last if($key =~ m|[0-9]| );
		# Save the result if tf >=5
		chomp(	my $count_ret = `perl /home/johntsai/law/get_count.pl $source $key`);
		my $count = int ($count_ret);	
		print "$source, $key($count)-\t$buffer\n\n";

		if($count>=5){
			#my $trash =<STDIN>;
			$i += 1;
			$line = $ret_lines[$i];
			until($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
				last if($line =~ m/0.000000/ eq "1");
				$buffer .= "$line\n";
				$i += 1;
				$line = $ret_lines[$i];
			}
			#chomp(my $trash=<STDIN>) if($ch_count{$key}>=5);
			$add->execute($source,$key,$buffer);
		}
	}
}
print STDERR "done.\n";
