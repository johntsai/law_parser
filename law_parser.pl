#!/usr/bin/perl -w
# Parse data in johntsai.law and update it back.

use utf8;
use encoding "utf8";
use lib '/home/johntsai/pm';
#use getCut;
use MyParse;
use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;
use Encode qw/from_to encode decode/;
#require "/home/johntsai/pm/stanford-parser.pm";

my $sth = $dbh->prepare("SELECT `id`, `source`, `ch`, `en` FROM `law` WHERE ch_parsed IS NULL");
$sth->execute();
my $update = $dbh->prepare("UPDATE law SET ch_parsed=? WHERE id=?");
while(my($id,$source,$ch,$en,$ch2)=$sth->fetchrow_array()){
	print STDERR "+$id\t$ch\n";
	my $cut = "error";
	while($cut =~ m|error|i){
		$cut = MyParse::ParseTrad($ch);
	}
	#$cut = $dbh->quote($cut);
	$update->execute($cut,$id);
	utf8::decode($cut);
	print STDERR $cut,"!!!\n";
}
print STDERR "done.\n";

=head
my $line = "昨天的事情，今天做完";
my $line5 = encode("big5",decode("utf-8",$line));
print utf8::is_utf8($line5),":$line5\n";
#my $line = "測試";
utf8::decode($line);
my @ret = getCut::getCut($line);
foreach (@ret){
	print "$_->{content}\n";
}
=cut
