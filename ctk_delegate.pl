#!/usr/bin/perl -w
#
# 1. Get ctk-aligned results -> johntsai.ctkAligner_ctkResults (divided by dbno)
# === $perl ctk_query_all.pl ctk.config
# 2. Parse all results by stanford Parser / /home/kk770614/public_html/cgi-bin/vnrel_all/index2.cgi and save into silentvow.Parser
# 3. Align all sentences by Berkerley word Alignment -> silentvow.Aligner
# 4. Find out word pair and get LLR 
# Set in ctk.config to set param if you want to use command line mode.
# Usage: `ctk.pl [config_file]` 
# 

require '/home/silentvow/public_html/cgi-bin/collocation/work17-lib_final.pl';
use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use HTML::Entities;

use lib '/home/denehs/public_html/cgi-bin/modules/';
use ZH::Ckipsvr;
use WordNet::QueryData;
use lib '/home/johntsai/pm';
use getCut;
use DBI;
use Try::Tiny;

# GLOBAL
my %param;
my $query_id = -1;
my $config_file;
@dblist = ("index/novel", "index/news", "index/novel2", 
	"index/sinorama", "index/novel3", "index/new", "index/concise", 
	"index/LDC2006T04", "index/concise2", "index/scientific_american", "index/env",
	"index/bible-kjv", "index/bible-bbe", "index/bom", "index/bible-combined",
);
@dbname = (
	'英文小說',
	'VOA',
	'英文小說(2)',
	'光華雜誌',
	'英文小說(3)',
	'new data',
	'concise encyclopedia',
	'LDC2006T04',
	'concise encyclopedia(有斷句)',
	'科學人',
	'ENV環境新聞',
	'BIBLE(King James Version)',
	'Bible in Basic English',
	'Book of Mormon',
	'Bible(combined)',
);
@all_param = (
	'word',
	'lang',
	'db',	
	'option',
	'lexical',
	'fuzzy',
	'count_stat',
);
=head
# I've decided to go on it after finishing step4
sub parse_ch{
	my $input = shift;
	my $path = "/home/kk770614/public_html/cgi-bin/vnrel_all/index2.cgi";
	my $content = `$path $input`;
	#TODO parse usefull information 
}
=cut
sub np_chunk{
	my $q = shift;
	my $ret = `/home/kk770614/public_html/cgi-bin/vnrel_all/index2.cgi $q`;
}
sub set_parser_config{
	my $optRef = shift;
	my $config_file = shift;
	open (FD,">$config_file");
	print FD "typedDependencies:1\n";
	print FD "penn:0\n";
	print FD "show:0\n";
	print FD "wordsAndTags:0\n";
	if($optRef->{'lang'} eq '1'){	# english
		print FD "lang:en\n";
		print FD "parser:englishPCFG.ser.gz\n";
	}else{
		print FD "lang:tc\n";
		print FD "parser:xinhuaPCFG.ser.gz\n";
		print FD "segmenter:ckipsvr\n";
	}
	close(FD);
}
sub print_start{
	$title = $_[0];
	if( !defined($title) ){ $title = 'Translation search result';}
	print header(-type => 'text/html', -charset => 'utf8'),
	start_html(-lang => 'zh-TW', -encoding => 'utf8',
		-title => $title),h3($title),p,"\n";
}

sub get_total_count{
	my $count = 0;
	my %all = %{$_[0]};
	foreach my $k (keys %all){
		if($all{$k}->{V} != -1){
			$count++;
		}
		if($all{$k}->{N} != -1){
			$count++;
		}
		if($all{$k}->{A} != -1){
			$count++;
		}

	}
	return $count;
}

sub paint_color{
	$short = $_[0];
	$long = $_[1];
	$color = $_[2];

	$short=~ s/\s/ /g;
	$long =~ s/\s/ /g;
	my $pos1 = index( $long , $short) ;
	my $en;
	$en = substr($long,0,$pos1);
	$en .= "<font color = $color>";
	$en .= substr($long,$pos1,length($short));
	$en .= "</font>";
	$en .= substr($long,$pos1+length($short));

	return $en;

}

sub make_keyword_query {
	my $q = shift;
	$q =~ s/([\+\-\&\|\!\(\)\{\}\[\]\^\"\~\*\?\:\\])/\\$1/g;
	my @query = split /,/, $q;

	my $str = '';
	for (@query) {
		$str = $str . "english:\"$_\" chinese:\"$_\" ";
	}
	$str =~ s/\'/\'\"\'\"\'/g;

	return $str;
}

sub make_line_query {
	my ($title, $lineno) = @_;
	_utf8_on $title;
	$title =~ s/([\+\-\&\|\!\(\)\{\}\[\]\^\"\~\*\?\:\\])/\\$1/g;

	return "title:\"$title\" AND line:$lineno";
}

sub tolemma
{
	my ($word, $tag, $wn) = @_;

	my @ary = $wn->validForms ("$word#$tag");

	if (scalar(@ary)==0)
	{
		return "?";
	}

	my $min = 100;
	my $ans;

	foreach my $i (@ary)
	{
		if (length($i)<=$min)
		{
			$min = length($i);
			$ans = $i;
		}
	}

	$ans =~ /(.*?)#.*/;
	$ans = $1;

	return $ans;
}

sub align {

	chdir("/home/johntsai/public_html/alignment");
	system('rm','-rf','output');

	chdir("/home/johntsai/berkeleyaligner");
	#system('./align','example.conf');
	my $cmd = "./align example.conf";
	my $align_msg = `$cmd`;

	return 0;
}

sub addlink
{
	my ($keyword, $line) = @_;
	my @word = split(/\s+/, $line);
	$word[1] =~ s/://;
	$keyword =~ s/\s+/_/g;
	my $url = "http://140.112.185.89/~silentvow/cgi-bin/ctk_with_gdic/ctk.pl?db=-1&query=$keyword&sub=$word[1]";

	return "<a href=$url>".$word[1]."</a>: ".$word[2]."<br>\n";
}

sub isstop_ch
{
	my ($word, $tag) = @_;
	if($word eq "aaaaaaaaaaaa")
	{
		return $word;
	}

	if($tag =~ m/CATEGORY/ eq "1")
	{
		return "";
	}
	my @array = ('C', 'D');
	my $pos = substr($tag, 0, 1);
	foreach (@array)
	{
		if($pos =~ m/$_/ eq "1")
		{
			return "";
		}
	}
	@array = ('P', 'T', 'I', 'FW', 'NF', 'NB', 'NH', 'NC', 'ND', 'NE', 'V_11', 'V_2');
	foreach (@array)
	{
		if($tag eq $_)
		{
			return "";
		}
	}

	#utf8::encode($word);
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-ch.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		#print STDERR "$_ $word\n";
		#chomp(my $trash=<STDIN>);
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}


	close STOP;
	return $word;
}

sub isstop_en
{
	my ($word) = @_;
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-en.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}

	close STOP;
	return $word;
}

sub aligner
{
	my ($keyword, $tag,$tmpfile, @lines) = @_;
	print STDERR "[DEBUG]311: Aligner Called - $keyword, $tag, $tmpfile<br>\n";
	#print STDERR "[DEBUG]311: Aligner Called - $keyword, $tag, $tmpfile, ",join("<br>\n",@lines),"<br>\n";
	my $final = "";

	my $dbh = DBI->connect("DBI:mysql:database=silentvow;host=mysql;mysql_enable_utf8=1", 'silentvow', 'foxtail'), @result;
	utf8::encode($keyword);
	#print "$var1<br>\n$var2<br>\n";
	my $sth = $dbh->prepare("select content from Aligner where keyword=? AND tag=?");
	$sth->execute($keyword,$tag);

# Fetch Result From Previous Job
	if(@result = $sth->fetchrow)
	{
		#print "[DEBUG][294:found] $result[0]<br>\n";
		if($result[0] =~ m|\w|){
			# remove something strange first
			my $_ = $result[0];
			$_ =~ s|^\'||;
			$_ =~ s|\'$||;
			$_ =~ s|\\n|\n|g;
			return $_ ;
			# REMOVE EMPTY ROWS
		}else{
			$dbh->prepare("DELETE FROM `Aligner` where keyword=? AND tag=?");
			$dbh->execute($keyword,$tag);
		}
	}

	#my @results = split(/\n/, $lines);
	my $replaceword = "aaaaaaaaaaaa";
	my $en_file = "/tmp/querylog2/en.tmp";
	my $ch_file = "/tmp/querylog2/ch.tmp";
	my $wn = WordNet::QueryData->new(dir => '/usr/share/wordnet/');

	chdir("/tmp/querylog2")|mkdir("/tmp/querylog2", 0777);
	open(TAG_E, ">$en_file");
	open(TAG_F, ">$ch_file");
	utf8::decode($keyword);

	my $index = 0;
	foreach my $sentence (@lines) 
	{

		my $string1 = $sentence->{string1};	# english string 
		my $string2 = $sentence->{string2};	# chinese string
		#print "-",$string1."\n";
		if($string1 !~ m|\w| || $string1 eq "0")
		{
			next;
		}
		#if($string2 !~ m|\w| || $string2 eq "0")
		if( $string2 eq "0")
		{
			print "\tXD\n";
			next;
		}
		try{
			my $str1 = lc($string1);
			#print "=",$str1,"\n";
			$str1 =~ s/[[:punct:]]//g;
			print STDERR "+",$str1,"\n";

			my $tmpstr = $string2;
			#$tmpstr =~ s/[a-zA-Z]//g;
			$tmpstr =~ s/$keyword/ $replaceword /g;
			$tmpstr =~ s/\W+/ /g;		#revmoe punctuation marks, but it does not work on the chinese ones!
			#@sets = split(/\s+/,$tmpstr);

			my $str2 = "";
			#utf8::encode($tmpstr);
			# remove punctuations first
			print STDERR "+",$tmpstr,"\n";
			my @cuts = getCut::getCut($tmpstr);
			foreach my $word (@cuts)
			{
				my $content = $word->{content};
				# remove html tags
				next if($content =~ m|<pre>|);
				$content = isstop_ch($word->{content}, $word->{tag}) if ($tag =~ /nof/);
				$content =~ s|</pre>.*$||;
				# XXX Seems that isstop ch is not working
				#print STDERR "$word->{content},$tag";
				if($content ne "")
				{
					# print STDERR "$content\_";
					$str2 .= "$content ";
				}else{
					#print STDERR "STOP! \n";
					;
				}
				#chomp(my $trash=<STDIN>);
				# print STDERR "\n";
				#$str2 .= $word->{-content}." ";
			}
			#print $string2."<br>\n";
			#print $str2."<br>\n";
=head
	my $cuts = $ckipsvr->parse($unit);
				if($cuts eq "Unable to connect to remote host.")
				{
					;
				}
				print $unit.$cuts."<br>\n";
				foreach my $cut (@$cuts)
				{
					print $cut;
					foreach my $word (@$cut)
					{
						my $content = isstop_ch($word->{-content}, $word->{-tag});
						if($content ne "")
						{
							$str2 .= "$content ";
						}
						#$str2 .= $word->{-content}." ";
					}
				}
=cut
#}

			# print STDERR "+$str2\n";
			print TAG_E $str1."\n";
			print TAG_F $str2."\n";

			$index += 2;
		}catch {
			warn "caught error: $_"; # not $@
		};

	}
	close TAG_E;
	close TAG_F;

	open(TAG_F, "<$ch_file");
	open(REQ_E, ">/tmp/querylog2/query.e");
	open(REQ_F, ">/tmp/querylog2/query.f");

	# Tag of English content, success
	chdir "/home/denehs/stanford/postagger-2006-05-21";

	# XXX Something wrong here
	print STDERR "[LOG]\tStart Parsing\n";
	my $tagger = "java -mx300m -classpath postagger-2006-05-21.jar edu.stanford.nlp.tagger.maxent.MaxentTagger -model wsj3t0-18-bidirectional/train-wsj-0-18 -file $en_file";
	my $result = `$tagger`;
	#print "$result<br>\n";
	# XXX Good until this step

	$i = 0;
	my @tagline = split(/\n/,$result);
	foreach $line (@tagline)
	{
		my @tags = split(/\s+/,$line);

		my $lemma = "";

		foreach $pair (@tags)
		{
			@data = split(/\//,$pair);
			$pos = substr $data[1], 0, 1;

			my $temp;
			if($pos eq "V")
			{
				$temp = tolemma ($data[0], 'v', $wn);
			}
			elsif($pos eq "N")
			{
				$temp = tolemma ($data[0], 'n', $wn);
			}
			elsif($pos eq "J")
			{
				$temp = tolemma ($data[0], 'a', $wn);
			}
			elsif($pos eq "R")
			{
				$temp = tolemma ($data[0], 'r', $wn);
			}
			else
			{
				$temp = '?';
			}

			if($temp eq '?')
			{
				$lemma .= $data[0]." ";
			}
			else
			{
				$lemma .= $temp." ";
			}

		}

		my $chsent = <TAG_F>;
		chomp($chsent);
		print STDERR "EN = \n",$lemma,"\n";
		print STDERR "CH = \n",$chsent,"\n";
		if($lemma !~ m|\w| || $lemma eq "0")
		{
			print STDERR "Empty:$lemma\n";
			next;
		}
		if($chsent !~ m|\w|)
		{
			print "Empty:$chsent\n";
			next;
		}
=head
=cut

$lemma =~ s/ $keyword / $replaceword /g;

@tags = split(/\s+/,$lemma);
$lemma = "";
foreach my $word (@tags)
{
	my $content = isstop_en($word);
	$lemma .= "$content ";
}
if($lemma !~ m|\w| || $lemma eq "0")	# prevent printing lines with only stop word
{
	next;
}
my $line1 = "<s snum=".(1001+$i)."> ".$lemma." </s>\n";
my $line2 = "<s snum=".(1001+$i)."> ".$chsent." </s>\n";
print REQ_E $line1;
print REQ_F $line2;

$i++;
	}
	close REQ_E;
	close REQ_F;

	print STDERR "[LOG]\tDoing Alignment\n";

	chdir("/home/johntsai/public_html/alignment");
	system('rm','-rf','output');
	chdir("/home/johntsai/berkeleyaligner");
	my $cmd = "./align example.conf";
	my $align_msg = `$cmd`;
	#print $align_msg,"\n";

	chdir("/home/johntsai/public_html/alignment/output");
	my $langid = $param{"lang"};
	open(ALIGN, "<$langid.params.txt");
	open(RLT, ">$tmpfile");

	my $flag = 0;
	print "<b>Word alignment result </b><br>\n";

	# Find the keyword
	while($_ = <ALIGN>)
	{
		#print $_,"\n" if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
		#print "w\n";
		if($_ =~ m/$replaceword/ eq "1" && $_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1")
		{
			$flag = 1;
			$_ =~ s/$replaceword/$keyword/g;
			$final .= "$_";
			last;
		}
	}
	# save all the results~ until the next one
	while($_ = <ALIGN>)
	{
		last if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
		last if($_ =~ m/0.000000/ eq "1");
		$final .= $_;
	}
	close ALIGN;

	#my $var3 = $dbh->quote($final);
	#$dbh->prepare("insert into Aligner (keyword, tag, content) values (?, ?, ?)")->execute($keyword,$tag,$final) if($final =~ m|\w|);

	#print "<hr>\n CTK Alignment Result:<br>\n".$final,"<hr>\n";
	#print "<hr>\n CTK Alignment Result:<br>\n".$var3,"<hr>\n";

	#print STDERR "Aligner return: ",$final,"\n";
	return $final;
}

sub color_sent{
	my $s = $_[0];
	my $w = $_[1];
	my $color = $_[2];
	$s =~ s/(\W)($w)(\W)/$1<font color = $color>$2<\/font>$3/gi;
	$s =~ s/($w)(\W)/<font color = $color>$1<\/font>$2/gi;
	return $s;

}
#
#start=============>

# set config first
if(@ARGV>0){
	open(OPT,"<$ARGV[0]");
	$config_file = $ARGV[0];
	while(<OPT>){
		$_ =~ s|\s*#.*$||;
		my $line = $_;
		$line =~ s|\s*$||;
		# remove array method of opt
=head 
		if($line =~ m|\;|){
			my @tmp;
			while($line =~ s|:([^\;]+)|:|){
				#print $1,"\n";
				push @tmp,$1;
			}
			$param{$1} = \@tmp if($line =~ m|^(.*?):|);
		}else{
=cut
if($line =~ m|^(.+?):(.+)|){
	$param{$1} = $2 ;
}elsif($line =~ m|^(.+?):|){
	$param{$1} = "" ;
}
#}
	}
	close(OPT);
	$query_id = $ARGV[1] if(@ARGV>1);
}else{
	die "Please input the config file.\nUsage: perl ctk_delegate.pl [config_file]";
}
=head
foreach my $key (keys(%param)){
	if($key eq "option"){
		print $key," => \n\r";
		my $arrayRef = $param{$key};
		print join ("\n\r",@$arrayRef),"\n\r";
	}else{
		print "$key => $param{$key}\r\n";
	}
}
=cut

#第一次submit後進入
if($param{'word'}){

# 1. Get ctk-aligned results -> johntsai.ctkAligner_ctkResults (divided by dbno)
# === $perl ctk_query_all.pl ctk.config
	`perl ctk_query_all.pl $config_file`;

	_utf8_on $param{'word'};
	my @opt = split(/;/,$param{'option'});
	my $func = "";
	my $signch = "";
	foreach(@opt)
	{ 
		print STDERR $_,"\n";
		$func = "nof" if ($_ =~ /functor/); 
		$signch = "on" if ($_ =~ /signch/); 
	}
	#chomp(my $trash=<STDIN>);
	# select db 
	my %dbs, %dbs_index; #the second one holds the index of selected database
	my %db_maxDoc; #$db_maxDoc{value of %dbs} = maxDoc of db
=head
	print "\n\n=dblists\n";
	foreach my $item (@dblist){
		print "=$item\n";
	}
=cut
if (defined($param{'db'}) && $param{'db'} eq '-1') {
	$dbs{$_} = $dblist[$_] for (0..$#dblist);
	$dbs_index{$dblist[$_]} = $_ for (0..$#dblist);

	# XXX hack for removing this database
	delete $dbs{6};
	delete $dbs_index{'concise encyclopedia'},
} else {
	my $db = (defined $dblist[$param{'db'}])?$dblist[$param{'db'}]:$dblist[0];
	my $dbno = (defined $dblist[$param{'db'}])?$param{'db'}:0;
	$dbs{$dbno} = $db;
	$dbs_index{$db} = $dbno;
}

# read the maxDoc.txt
open(MAXDOC,"</home/johntsai/public_html/cgi-bin/ctk/ctkAligner/maxDoc.txt");
while(<MAXDOC>){
	$_ =~ m|^(.+?)\s*\:\s*(\d+)$| ;
	$db_max{$1} = $2;
}
close(MAXDOC);

chdir("/tmp/querylog2")|mkdir("/tmp/querylog2", 0777);
my $keyword = $param{'word'};
#print "[Debug J]\t792:\tkeyword = $keyword<br>\n";

chdir ("/home/denehs/stanford/stanford-parser-2006-06-11");

my $index = 0;
#my $tmpname = "/tmp/stanford.in";
my $tmpname = "/home/johntsai/stanford.in";
my $langid = $param{'lang'};

my $tmp_config = "/tmp/stanford.config";
&set_parser_config(\%param,$tmp_config);
$cmd = "perl /home/johntsai/public_html/cgi-bin/ctk/ctkAligner/stanford-parser_cmd.pl $tmpname $tmp_config";
my %hash;
my %hashcount;
my $dbh = DBI->connect("DBI:mysql:database=silentvow;host=mysql;mysql_enable_utf8=1", 'silentvow', 'foxtail'), @result;
my $dbh2 = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
my @mydeps = ('prt', 'amod', 'dobj', 'advmod', 'prep', 'pobj',);

######TESTHEAD
my @lines;
######TESTEND

# Step 2. Parse all results by stanford Parser / http://140.112.185.101/~kk770614/cgi-bin/vnrel_all/index2.cgi?np=0 and save into silentvow.Parser

my $sth ;
if($param{'db'}==-1){
	$sth = $dbh2->prepare("SELECT `query`, `db`, `count`, `result` FROM `ctkAligner_ctkResult` WHERE query LIKE ?");
	$sth->execute($param{'word'});
}else{
	$sth = $dbh2->prepare("SELECT `query`, `db`, `count`, `result` FROM `ctkAligner_ctkResult` WHERE query LIKE ? and db in (?)");
	$sth->execute($param{'word'},join(',',values(%dbs_index)));
}
#print STDERR "\n\n$param{'word'}-\n",join(',',values(%dbs_index)),"\n===\n";
#for my $result (@results) 
while(my($query,$db,$count,$content) = $sth->fetchrow_array())
{
	print STDERR "\n\n$content\n";
	$content =~ s|\n|#|g;
	print "<hr>\n";
	print '<b>', $dbname[$db], "</b><br>\n";
	print '<b>共有', $count, "項搜尋結果</b><br>\n";

	#print "<ul>\n";s
	#for my $sentence (@{ $result->{sentences} }) 
	while($content ne "")
	{
		last unless($content =~ s|([^\#]+)#([^#]+)||);
		my $string1 = $1;
		my $string2 = $2;
		$string1 =~ s/<br>//g;
		$string2 =~ s/<br>//g;
		$string2 =~ s/[a-z]//gi;
		$string2 =~ s/[[:punct:]]//g;

		my $tmp;
		$tmp->{string1} = $string1;
		$tmp->{string2} = $string2;
		push @lines, $tmp;

=head
			#Something useless.
			my $line = $sentence->{line};
			my $title = $sentence->{title};
			$title =~ s/'/\\'/g;
			my $index2 = $index + 1;
			#print "<li><div class='title'>title: $title</div>\n";
=cut

$string1 =~ s/\s+/ /g;

#print "<div id='d$index'><b>line $line:</b> $string1<br></div>\n";
#print "<div id='d$index2'><b>line $line:</b> $string2<br></div>\n";
#print "<br>\n";

$index += 2;

my $input;
if($langid eq "1"){
	$input = $dbh->quote($string1);
}else{
	$input = $dbh->quote($string2);
}
#print "[DEBUG] Fetch $input\n\r";
# It will find if there's any words related to keyword, 
=head
			# Find if there's existing result from silentvow.Parser
			my $sth = $dbh->prepare("select content from Parser where sentence=?");
			$sth->execute($input);
			my $dep;
			if(@result = $sth->fetchrow)
			{
				$dep = $result[0];
				# Remove empty result
				if($result[0] !~ m|\w|){
					my $del = $dbh->prepare("delete from Parser where sentence=?");
					$del->execute($input);
				}
				# Fix some strange quotes....
				$dep =~ s|^\'||;
				$dep =~ s|\'$||;
				$dep =~ s|\\n|\n|g;
			}
			else  	# I have moved the ckipsvr step into parser_cmd
			{
				if($langid eq "2"){
					$input = $string2;
				}else{
					$input = $string1;
				}
				#$input = encode('GB18030', decode('UTF-8', $input));
				open FD, ">$tmpname";
				print FD $input; 
				close FD;
				$dep = `$cmd`;
				#$tmpdep = $dbh->quote($dep);
				utf8::decode($dep);
				#print "[DEBUG] $input\r\n";
				#print "[DEBUG] $dep\r\n";

				$dbh->prepare("insert into Parser (sentence, content) values (?,?)")->execute($input, $dep);
			}
			$string1 =~ s/"/\"/g;
			$string2 =~ s/"/\\\"/g;
			my @deps = split(/\n/,$dep);
			my $resKey = quotemeta $keyword;
			foreach my $d(@deps)
			{
				#last if($count>200);
				utf8::decode($d);
				#print utf8::is_utf8($d)," $resKey VS $d <br>\n";
				#if($d =~ /(\w+)\((\S+?)-(\d+), (\S+?)-(\d+)/g)
				if($d =~ m|$resKey|)
				{
					#print "[DEBUG] ",$d, " + <br>\n";
					$d =~ /(\w+)\((\S+?)-(\d+), (\S+?)-(\d+)/g;
					$a = $2;
					$b = $4;

					foreach(@mydeps)
					{
						if($1 eq $_)
						{
							if(($keyword =~ /$a/ and $a =~ m/$keyword/) && ($keyword =~ /$b/ and $b =~ m/$keyword/))
							{
								next;
							}
							if($keyword =~ /$a/ and $a =~ m/$keyword/)
							{
								if($hash{$b} eq "")
								{
									$hashcount{$b} = 0;
								}
								$hash{$b} = "$hash{$b}$string1\n$string2\n\n";
								++$hashcount{$b};
								++$count;
								#print "[DEBUG]======$hash{$b}<br>\n";
								last;
							}
							if($keyword =~ /$b/ and $b =~ m/$keyword/)
							{
								if($hash{$a} eq "")
								{
									$hashcount{$a} = 0;
								}
								$hash{$a} = "$hash{$a}$string1\n$string2\n\n";
								++$hashcount{$a};
								++$count;
								#print "[DEBUG]======$hash{$a}<br>\n";
								last;
							}
						}
					}
					#print "[DEBUG] ",$1," -> ", $a, ", ", $3, ", ", $b, ", ", $5, "<br>\n";
				}

			}
=cut
		}
		#print "</ul>\n";
	}
	print '</form>';

	print "<b>Word alignment result </b><br>\n";
=head
	print "[DEBUG]\t954:\tbefore aligner: $keyword<br>\n";
	print "\t\t\t$func<br>\n";
	foreach my $item(@lines){
		print "$item->{'string1'}<br>\n || <br>\n$item->{'string2'}<br>\n<br>\n===<br>\n";
	}
=cut
my $tmpfile = "/tmp/querylog2/".$param{'word'}.".rlt";
$tmpfile =~ s/\s+/+/g;
#print STDERR "FUNC -$func!\n";
#chomp(my $trash = <STDIN>);
$ret = aligner($keyword, "-$func",$tmpfile, @lines);
#print $ret,"<br>\n";
@aligns = split(/\n/, $ret);
my $web = "";
foreach(@aligns)
{
	#print "$_<br>\n";
	if($_ =~ m/entropy/ && $_ =~ m/nTrans/ && $_ =~ m/sum/)
	{
		#print "$_<br>\n";
		next;
	}
	my $keylex = ""; if($param{'lexical'} ne "")
	{
		my @word = split(/\s+/, $_);
		$word[1] =~ s/://;
		#print $word[1],"\n";
		utf8::decode($word[1]);
		#print $word[1],"\n";
		my $cuts = $ckipsvr->parse($word[1]);
		$keylex = $param{'lexical'};
		foreach $cut(@$cuts)
		{
			foreach $word(@$cut)
			{
				$keylex = "" if($word->{-tag} =~ /$keylex/);
			}
		}
	}
	next if($keylex ne "");
	my $link = addlink($keyword, $_);
	print $link;
	$web .= $link;
}

print "<hr>";
# save the result back to johntsai.ctkAligner_result
if($web =~ m|\w|){
	utf8::decode($web) ;
	if($query_id==-1){
		my $add_result = $dbh2->prepare("INSERT INTO `ctkAligner_result`(`keyword`, `content`) VALUES (?,?)");
		$add_result->execute($keyword,$web);
	}else{
		my $update = $dbh2->prepare("UPDATE `ctkAligner_query` SET `result`=? WHERE id=?");
		$update->execute($web,$query_id);
	}
}

=head
	@sorted = sort { $hashcount{$b} <=> $hashcount{$a} } keys %hashcount;
	foreach(@sorted)
	{
		#print '<form name="form1" Method="POST" Action="ctk.pl">';
		#print '<input type="hidden" name="word" value="', $param{'word'}, '" />';
		#print '<input type="hidden" name="db" value="', $param{'db'}, '" />';
		print "<DL><DT><b><big>", $keyword, " (", $_, "): ", $hashcount{$_}, "</big></b><br>\n";
		my $al = "";
		my @w = split(/\n\n/, $hash{$_});
		my @alignset = ();
		if($hashcount{$_} > 1)
		{
			print "<b>Word Alignment Result</b><br>\n", "<DD>";
			@lines = ();
			foreach my $s (@w)
			{
				my @ss = split(/\n/, $s);
				my $tmp;
				$tmp->{string1} = $ss[0];
				$tmp->{string2} = $ss[1];
				push @lines, $tmp;
			}
			$ret = aligner($keyword, "$_ -$func",$tmpfile, @lines);
			if($ret eq "")
			{
				print "查無此關鍵字";
			}
			else
			{
				@aligns = split(/\n/, $ret);
				foreach my $a(@aligns)
				{
					$al = "$al$a\n";
					if($a =~ m/entropy/ && $a =~ m/nTrans/ && $a =~ m/sum/)
					{
						print "$a<br>\n";
						next;
					}
					my $keylex = "";
					if($param{'lexical'} ne "")
					{
						my @word = split(/\s+/, $a);
						$word[1] =~ s/://;
						utf8::decode($word[1]);
						my $cuts = $ckipsvr->parse($word[1]);
						$keylex = $param{'lexical'};
						foreach $cut(@$cuts)
						{
							foreach $word(@$cut)
							{
								$keylex = "" if($word->{-tag} =~ /$keylex/);
							}
						}
					}
					next if($keylex ne "");
					@aa = split(/:/, $a);
					utf8::decode($aa[0]);
					push @alignset, $aa[0] if ($signch eq "on");
					print addlink($keyword, $a);
				}
			}
		}
		#print '<input type="hidden" name="align" value=\'', $al, '\' />';


		print "<DT><b>Example Sentences</b><br>\n", "<DD>";
		foreach my $s (@w)
		{
			my @ss = split(/\n/, $s);
			my $s1 = $ss[0];
			my $s2 = $ss[1];
			#print "<b>$s1</b><br>\n$s2<br>\n";
			$s1 =~ s/\'/\^^^/g;
			$s2 =~ s/\'/\^^^/g;

			$s1 =~ s/ $keyword / <font color = red>$keyword<\/font> /gi;
			$s1 =~ s/ $_ / <font color = blue>$_<\/font> /gi;
			$s1 =~ s/ $keyword,/ <font color = red>$keyword<\/font>,/gi;
			$s1 =~ s/ $_,/ <font color = blue>$_<\/font>,/gi;
			$s1 =~ s/ $keyword./ <font color = red>$keyword<\/font>./gi;
			$s1 =~ s/ $_./ <font color = blue>$_<\/font>./gi;
			#print "<input type=\"checkbox\" name=\"sentence\" value=\'$s1\n$s2\'>\n";
			$s1 =~ s/\^\^\^/\'/g;

			utf8::encode($s2);
			@cuts = getCut::getCut($s2);
			utf8::decode($s2);
			my @verbs = ();
			foreach my $word (@cuts)
			{
				#print $word->{content}, "=", $word->{tag}, "<br>";
				if($word->{tag} =~ /V/)
				{
					utf8::decode($word->{content});
					push @verbs, $word->{content};
				}
			}
			#print "<br>\n";
			my $flag = 0;
			foreach my $a(@alignset)
			{
				foreach my $v (@verbs)
				{
					my $count = 0;
					my @tmp = split(//,$v);
					foreach(@tmp)
					{
						if($a =~ /$_/)
						{
							#print $a, "=", $_, "<br>\n";
							$count++;
						}
					}
					#print $a, "/", $v, "=", $count, ": ", length($a), "/", length($v), "<br>\n";
					if($count == length($a)-2)
					{
						$s2 =~ s/$v/<font color = red>$v<\/font>/g;
						$flag = 1;
						last;
					}
					elsif($count*10 > (length($v)+length($a)-2)/$param{'fuzzy'})
					{
						#print $count, "/", length($v), "/", length($a), "<br>\n";
						$s2 =~ s/$v/<font color = yellow>$v<\/font>/g;
						$flag = 1;
						last;
					}
				}
				last;
				last if($flag == 1);
			}

			#if($flag == 1){
				print $s1, "<br>\n";
				print $s2, "<br>\n";
				#}

		}
		print "</DL>\n";
		#print "<input type=\"submit\" name=\"ok_but\" value=\"highlight keywords\"><br>";
		#print '</form>';

=cut
=head
		$hash{$_} =~ s/ $keyword / <font color = blue>$keyword<\/font> /gi;
		$hash{$_} =~ s/ $_ / <font color = red>$_<\/font> /gi;
		$hash{$_} =~ s/ $keyword,/ <font color = blue>$keyword<\/font>,/gi;
		$hash{$_} =~ s/ $_,/ <font color = red>$_<\/font>,/gi;
		$hash{$_} =~ s/ $keyword./ <font color = blue>$keyword<\/font>./gi;
		$hash{$_} =~ s/ $_./ <font color = red>$_<\/font>./gi;
		$hash{$_} =~ s/\n+/<br>\n/g;
		print $hash{$_}, "<br></DL>\n";
=cut
#}
#print "<hr>";
#while (my ($key, $value) = each (%hash)) {  	
# 取出雜湊中的每一對鍵值，並且分別放入$key, $value
#	print "$key => $value\n";
#}

}
else
{
	print_start;
	print "Please enter at least one word.<br>\n";
}

