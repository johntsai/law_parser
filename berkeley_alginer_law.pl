#!/usr/bin/perl -w
# 1. Gather lines from johntsai.law from asigned source
# 2. Find Enlgish and Chinese words that appears more than 5 times.
# 3. Run BWA on all sentences and save the results of the words which are found in previous step.
# 4. Save them into DB: johntsai.BWA_law_parsed.
#
# note: johntsai.BWA_law saves the entire BWA result
#
# Usage: perl berkeley_aligner_law.pl <source_name>

require '/home/silentvow/public_html/cgi-bin/collocation/work17-lib_final.pl';
use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use HTML::Entities;

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;

use lib '/home/denehs/public_html/cgi-bin/modules/';
use ZH::Ckipsvr;
use WordNet::QueryData;
use lib '/home/johntsai/pm';
#use getCut;
use MyParse;

# GLOBAL VARS

sub isstop_en
{
	my ($word) = @_;
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-en.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}

	close STOP;
	return $word;
}
sub tolemma
{
	my ($word, $tag, $wn) = @_;

	my @ary = $wn->validForms ("$word#$tag");

	if (scalar(@ary)==0)
	{
		return "?";
	}

	my $min = 100;
	my $ans;

	foreach my $i (@ary)
	{
		if (length($i)<=$min)
		{
			$min = length($i);
			$ans = $i;
		}
	}

	$ans =~ /(.*?)#.*/;
	$ans = $1;

	return $ans;
}
sub align_all
{
	my @lines = @_;
	my $final = "";

	#my @results = split(/\n/, $lines);
	my $en_file = "/tmp/querylog_jt_law/en.tmp";
	my $ch_file = "/tmp/querylog_jt_law/ch.tmp";
	my $wn = WordNet::QueryData->new(dir => '/usr/share/wordnet/');

	chdir("/tmp/querylog_jt_law")|mkdir("/tmp/querylog_jt_law", 0777);
	open(TAG_E, ">$en_file");
	open(TAG_F, ">$ch_file");

	foreach my $sentence (@lines) 
	{
		my $string1 = $sentence->{string1};	# english string 
		my $string2 = $sentence->{string2};	# chinese string
		#print "-",$string1."\n";
		if($string1 !~ m|\w| || $string1 eq "0")
		{
			next;
		}
		#if($string2 !~ m|\w| || $string2 eq "0")
		if( $string2 eq "0")
		{
			print "\tXD\n";
			next;
		}
		my $str1 = lc($string1);
		#print "=",$str1,"\n";
		$str1 =~ s/[[:punct:]]//g;
		print STDERR "+",$str1,"\n";

		#my $tmpstr = $string2;
		#$tmpstr =~ s/[a-zA-Z]//g;
		$str2 = $string2;
		$str2 =~ s/\W+/ /g;		#revmoe punctuation marks, but it does not work on the chinese ones!
		#@sets = split(/\s+/,$tmpstr);

		# remove punctuations first
		print STDERR "+",$str2,"\n";
		# No need to getCut (since I've done it in law_parser.pl)
		#my $str2 = MyParse::ParseTrad($tmpstr);
		#print $str2,"!!!\n";
		#chomp(my $trash=<STDIN>);

# print STDERR "+$str2\n";
		print TAG_E $str1."\n";
		print TAG_F $str2."\n";
	}
	close TAG_E;
	close TAG_F;

	open(TAG_F, "<$ch_file");
	open(REQ_E, ">/tmp/querylog_jt_law/query.e");
	open(REQ_F, ">/tmp/querylog_jt_law/query.f");

	# Tag of English content, success
	chdir "/home/denehs/stanford/postagger-2006-05-21";

	# XXX Something wrong here
	print STDERR "[LOG]\tStart Parsing\n";
	my $tagger = "java -mx300m -classpath postagger-2006-05-21.jar edu.stanford.nlp.tagger.maxent.MaxentTagger -model wsj3t0-18-bidirectional/train-wsj-0-18 -file $en_file";
	my $result = `$tagger`;
	#print "$result<br>\n";
	# XXX Good until this step

	$i = 0;
	my @tagline = split(/\n/,$result);
	foreach $line (@tagline)
	{
		my @tags = split(/\s+/,$line);

		my $lemma = "";

		foreach $pair (@tags)
		{
			@data = split(/\//,$pair);
			$pos = substr $data[1], 0, 1;

			my $temp;
			if($pos eq "V")
			{
				$temp = tolemma ($data[0], 'v', $wn);
			}
			elsif($pos eq "N")
			{
				$temp = tolemma ($data[0], 'n', $wn);
			}
			elsif($pos eq "J")
			{
				$temp = tolemma ($data[0], 'a', $wn);
			}
			elsif($pos eq "R")
			{
				$temp = tolemma ($data[0], 'r', $wn);
			}
			else
			{
				$temp = '?';
			}

			if($temp eq '?')
			{
				$lemma .= $data[0]." ";
			}
			else
			{
				$lemma .= $temp." ";
			}

		}

		my $chsent = <TAG_F>;
		chomp($chsent);
		print STDERR "EN = \n",$lemma,"\n";
		print STDERR "CH = \n",$chsent,"\n";
		if($lemma !~ m|\w| || $lemma eq "0")
		{
			print STDERR "Empty:$lemma\n";
			next;
		}
		if($chsent !~ m|\w|)
		{
			print "Empty:$chsent\n";
			next;
		}

		#$lemma =~ s/ $keyword / $replaceword /g;

		@tags = split(/\s+/,$lemma);
		$lemma = "";
		foreach my $word (@tags)
		{
			my $content = isstop_en($word);
			$lemma .= "$content ";
		}
		if($lemma !~ m|\w| || $lemma eq "0")	# prevent printing lines with only stop word
		{
			next;
		}
		my $line1 = "<s snum=".(1001+$i)."> ".$lemma." </s>\n";
		my $line2 = "<s snum=".(1001+$i)."> ".$chsent." </s>\n";
		print REQ_E $line1;
		print REQ_F $line2;

		$i++;
	}
	close REQ_E;
	close REQ_F;

	print STDERR "[LOG]\tDoing Alignment\n";

	chdir("/home/johntsai/public_html/alignment");
	system('rm','-rf','output');
	chdir("/home/johntsai/berkeleyaligner");
	my $cmd = "./align law.conf";
	my $align_msg = `$cmd`;
	#print $align_msg,"\n";

	chdir("/home/johntsai/public_html/alignment/output");
	#my $langid = $param{"lang"};
	open RES_E, "<1.params.txt";
	open RES_F, "<2.params.txt";

	print "<b>Word alignment result </b><br>\n";

	# save all results
	while($_ = <RES_E>)
	{
		$final .= $_;
	}
	while($_ = <RES_F>)
	{
		$final .= $_;
	}
	close RES_E;
	close RES_F;

	#my $var3 = $dbh->quote($final);
	#$dbh->prepare("insert into Aligner (keyword, tag, content) values (?, ?, ?)")->execute($keyword,$tag,$final) if($final =~ m|\w|);

	#print "<hr>\n CTK Alignment Result:<br>\n".$final,"<hr>\n";
	#print "<hr>\n CTK Alignment Result:<br>\n".$var3,"<hr>\n";

	#print STDERR "Aligner return: ",$final,"\n";
	return $final;
}

# ===== start

#my $input_file;
my $source;
my $sth;
my %en_count,%ch_count;
my @lines;

# Load aligned lines from johntsai.law 
if(@ARGV>0){
	my $sth;
	$source = shift;
	$sth = $dbh->prepare("SELECT `id`, `en`, `ch_parsed` FROM `law` WHERE source LIKE ?");
	$sth->execute($source);
	while(my ($id,$en,$ch)=$sth->fetchrow_array()){
		print "$id:\n$en\n$ch\n=\n";

		if($en ne "" and $ch ne ""){
			$en =~ s|\r||g;
			$en =~ s|\n||g;
			$ch =~ s|\n||g;
			$ch =~ s|\r||g;
			# push results into @lines array
			my $tmp;
			$tmp->{string1} = $en;	# english string 
			$tmp->{string2} = $ch;	# ch
			push @lines, $tmp;

			# Calculate count of all words
			my @split = split /[^a-zA-Z]/,$en;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($en_count{$word} eq ""){
					$en_count{$word} = int(1);
				}else{
					$en_count{$word} += 1;
				}
			}

			utf8::decode($ch);
			@split = split /\s/,$ch;
			foreach my $word(@split){
				next if($word !~ m|\w|);
				if($ch_count{$word} eq ""){
					$ch_count{$word} = int(1);
				}else{
					$ch_count{$word} += 1;
				}
			}
		}
	}
}else{
	die "Usage: perl berkeley_aligner_law.pl <source_name>\n";
}

# Call Berkeley Word Aligner
$ret = align_all(@lines);
print $ret,"\n";

# Output the result to johntsai.BWA_law
my $sth=$dbh->prepare("INSERT INTO `BWA_law`(`source`, `content`) VALUES (?,?)");
$sth->execute($source,$ret);


# Find out results words w. tf >= 5
my @ret_lines = split /[\n\r]+/,$ret;
=head
foreach my $line(@ret_lines){
	print "$line\n";
	chomp(my$trash=<STDIN>);
}
=cut

my $add =$dbh->prepare("INSERT INTO `BWA_law_parsed`( `source`, `keyword`, `content`) VALUES (?,?,?,?)");
for(my $i=0; $i<=$#ret_lines ; $i++){
	my $buffer;
	my $line = $ret_lines[$i];
	# Find the result
	if($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
		#$buffer = "$line\n";
		$buffer = "";
		$line =~ m|^\s*(\S+)|;
		my $key = $1;
		if($en_count{$key}>=5 or $ch_count{$key}>=5){
			$i += 1;
			$line = $ret_lines[$i];
			until($line =~ m/entropy/ eq "1" && $line =~ m/nTrans/ eq "1" && $line =~ m/sum/ eq "1"){
				last if($line =~ m/0.000000/ eq "1");
				$buffer .= "$line\n";
				$i += 1;
				$line = $ret_lines[$i];
			}
			print "$source, $key-\t$buffer\n\n";
			#chomp(my$trash=<STDIN>);
			$add->execute($source,$key,$buffer);
		}
	}
}
print STDERR "done.\n";
