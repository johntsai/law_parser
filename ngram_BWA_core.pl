#!/usr/bin/perl -w
# 1. Find lines containing ngram query from johntsai.law
# 2. Run BWA program and find the corresponding result.

use Encode qw/encode decode _utf8_on/;
use Encode qw/from_to/;
use Encode::HanConvert;
use Fcntl qw(:seek);
use utf8;
use encoding "utf8";

use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use HTML::Entities;

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
use Try::Tiny;

use lib '/home/denehs/public_html/cgi-bin/modules/';
#use ZH::Ckipsvr;
use WordNet::QueryData;
use lib '/home/johntsai/pm';
use MyParse;

sub isstop_en
{
	my ($word) = @_;
	open(STOP, "</home/silentvow/public_html/cgi-bin/collocation/stoplist-en.txt");

	while($_ = <STOP>)
	{
		chomp($_);
		$_ =~ s/\s+//g;
		if($_ eq $word)
		{
			close STOP;
			return "";
		}
	}

	close STOP;
	return $word;
}
sub tolemma
{
	my ($word, $tag, $wn) = @_;

	my @ary = $wn->validForms ("$word#$tag");

	if (scalar(@ary)==0)
	{
		return "?";
	}

	my $min = 100;
	my $ans;

	foreach my $i (@ary)
	{
		if (length($i)<=$min)
		{
			$min = length($i);
			$ans = $i;
		}
	}

	$ans =~ /(.*?)#.*/;
	$ans = $1;

	return $ans;
}
sub aligner
{
	my ($keyword, $source, @lines) = @_;
	$keyword =~ s|^\s+||;
	$keyword =~ s|\s+$||;
	print STDERR "[DEBUG]311: Aligner Called - $keyword<br>\n";
	#print STDERR "[DEBUG]311: Aligner Called - $keyword, $tag, $tmpfile, ",join("<br>\n",@lines),"<br>\n";
	my $final = "";

	my $dbh = DBI->connect("DBI:mysql:database=johntsai;host=mysql;mysql_enable_utf8=1","johntsai","QmtEVj58uRwM8QEd");
	utf8::encode($keyword);
	#print "$var1<br>\n$var2<br>\n";
	my $sth = $dbh->prepare("select content from law_aligner where keyword=? AND source=?");
	$sth->execute($keyword,$source);

	# Fetch Result From Previous Job
	if(my @result = $sth->fetchrow)
	{
		if($result[0] =~ m|\w|){
			my $_ = $result[0];
			$_ =~ s|^\'||;
			$_ =~ s|\'$||;
			$_ =~ s|\\n|\n|g;
			return $_;
		}else{ # REMOVE EMPTY ROWS
			$dbh->prepare("DELETE FROM `law_aligner` where keyword=? AND source=?");
			$dbh->execute($keyword,$source);
		}
	}

	#my @results = split(/\n/, $lines);
	my $replaceword = "aaaaaaaaaaaa";
	my $en_file = "/tmp/querylog2/en.tmp";
	my $ch_file = "/tmp/querylog2/ch.tmp";
	my $wn = WordNet::QueryData->new(dir => '/usr/share/wordnet/');

	chdir("/tmp/querylog2")|mkdir("/tmp/querylog2", 0777);
	open(TAG_E, ">$en_file");
	open(TAG_F, ">$ch_file");
	utf8::decode($keyword);

	foreach my $sentence (@lines) 
	{
		my $string1 = $sentence->{string1};	# english string 
		my $string2 = $sentence->{string2};	# chinese string
		#print "-",$string1."\n";
		if($string1 !~ m|\w| || $string1 eq "0")
		{
			next;
		}
		#if($string2 !~ m|\w| || $string2 eq "0")
		if( $string2 eq "0")
		{
			print STDERR "\tXD\n";
			next;
		}
		my $str1 = lc($string1);
		$str1 =~ s/$keyword/$replaceword/ig;

		#print "=",$str1,"\n";
		$str1 =~ s/[[:punct:]]//g;
		print STDERR "+",$str1,"\n";

		my $str2 = $string2;
		utf8::decode($str2);
		#$str2 =~ s/[a-zA-Z]//g;
		$str2 =~ s/$keyword/ $replaceword /g;
		#@sets = split(/\s+/,$str2);

		# remove punctuations first
		#print STDERR "+",$str2,"\n";
		# Parse chinese line
		my $try_count = 0;
		my $cut = "error";
		while($cut =~ m|error|i){
			$cut = MyParse::ParseTrad($str2);
			$try_count ++;
			last if($try_count >5);
		}
		# if try_count>5: ...
		#$cut = $dbh->quote($cut);
		utf8::decode($cut);
		$cut  =~ s/\W+/ /g;		#revmoe punctuation marks
		print STDERR "+",$str2,"\n-> $cut\n";
		$str2 = $cut;
		#print $string2."<br>\n";
		#print $str2."<br>\n";

		print TAG_E $str1."\n";
		print TAG_F $str2."\n";
	}
	close TAG_E;
	close TAG_F;

	open(TAG_F, "<$ch_file");
	open(REQ_E, ">/tmp/querylog2/query.e");
	open(REQ_F, ">/tmp/querylog2/query.f");

	# Tag of English content, success
	chdir "/home/denehs/stanford/postagger-2006-05-21";

	print STDERR "[LOG]\tStart Parsing\n";
	my $tagger = "java -mx300m -classpath postagger-2006-05-21.jar edu.stanford.nlp.tagger.maxent.MaxentTagger -model wsj3t0-18-bidirectional/train-wsj-0-18 -file $en_file";
	my $result = `$tagger`;
	print "$result<br>\n";

	$i = 0;
	my @tagline = split(/\n/,$result);
	foreach $line (@tagline)
	{
		my @tags = split(/\s+/,$line);

		my $lemma = "";

		foreach $pair (@tags)
		{
			@data = split(/\//,$pair);
			$pos = substr $data[1], 0, 1;

			my $temp;
			if($pos eq "V")
			{
				$temp = tolemma ($data[0], 'v', $wn);
			}
			elsif($pos eq "N")
			{
				$temp = tolemma ($data[0], 'n', $wn);
			}
			elsif($pos eq "J")
			{
				$temp = tolemma ($data[0], 'a', $wn);
			}
			elsif($pos eq "R")
			{
				$temp = tolemma ($data[0], 'r', $wn);
			}
			else
			{
				$temp = '?';
			}

			if($temp eq '?')
			{
				$lemma .= $data[0]." ";
			}
			else
			{
				$lemma .= $temp." ";
			}

		}

		my $chsent = <TAG_F>;
		chomp($chsent);
		print STDERR "EN = \n",$lemma,"\n";
		print STDERR "CH = \n",$chsent,"\n";
		if($lemma !~ m|\w| || $lemma eq "0")
		{
			print STDERR "Empty:$lemma\n";
			next;
		}
		if($chsent !~ m|\w|)
		{
			print STDERR "Empty:$chsent\n";
			next;
		}

		$lemma =~ s/ $keyword / $replaceword /g;

		@tags = split(/\s+/,$lemma);
		$lemma = "";
		foreach my $word (@tags)
		{
			my $content = isstop_en($word);
			$lemma .= "$content ";
		}
		if($lemma !~ m|\w| || $lemma eq "0")	# prevent printing lines with only stop word
		{
			next;
		}
		my $line1 = "<s snum=".(1001+$i)."> ".$lemma." </s>\n";
		my $line2 = "<s snum=".(1001+$i)."> ".$chsent." </s>\n";
		print REQ_E $line1;
		print REQ_F $line2;

		$i++;
	}
	close REQ_E;
	close REQ_F;

	print STDERR "[LOG]\tDoing Alignment\n";

	chdir("/home/johntsai/public_html/alignment");
	system('rm','-rf','output');
	chdir("/home/johntsai/berkeleyaligner");
	my $cmd = "./align example.conf";
	my $align_msg = `$cmd`;
	#print $align_msg,"\n";

	chdir("/home/johntsai/public_html/alignment/output");
	#my $langid = $param{"lang"};
	open RES_E, "<1.params.txt";
	open RES_F, "<2.params.txt";

	my $flag = 0;
	print STDERR "<b>Word alignment result </b><br>\n";

	# Find the keyword in English result
	while($_ = <RES_E>)
	{
		#print $_,"\n" if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
		#print "w\n";
		if($_ =~ m/\b$replaceword\b/ eq "1" && $_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1")
		{
			$flag = 1;
			$_ =~ s/$replaceword/$keyword/g;
			$final .= "$_";
			last;
		}
	}
	# save all the results~ until the next one
	while($_ = <RES_E>)
	{
		last if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
		last if($_ =~ m/0.000000/ eq "1");
		$final .= $_;
	}
	close RES_E;
	# Find the keyword in Chinese result
	unless($flag){
		while($_ = <RES_F>)
		{
			#print $_,"\n" if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
			#print "w\n";
			if($_ =~ m/$replaceword/ eq "1" && $_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1")
			{
				$flag = 1;
				$_ =~ s/$replaceword/$keyword/g;
				$final .= "$_";
				last;
			}
		}
		# save all the results~ until the next one
		while($_ = <RES_F>)
		{
			last if($_ =~ m/entropy/ eq "1" && $_ =~ m/nTrans/ eq "1" && $_ =~ m/sum/ eq "1");
			last if($_ =~ m/0.000000/ eq "1");
			$final .= $_;
		}
	}
	close RES_F;

	#my $var3 = $dbh->quote($final);
	if($flag == 1){
		my $update=$dbh->prepare("insert into johntsai.law_aligner (keyword, source, content) values (?, ?, ?)") ;
		$update->execute($keyword,$source,$final)
	}

	#print "<hr>\n CTK Alignment Result:<br>\n".$final,"<hr>\n";
	#print "<hr>\n CTK Alignment Result:<br>\n".$var3,"<hr>\n";

	print STDERR "Aligner return: ",$final,"\n";
	return $final;
}

# ===== start

#my $input_file;
my ($q,$source);
my (%en_count,%ch_count);
my @lines;

# Load aligned lines from johntsai.law 
if(@ARGV< 2){
	die "Usage: perl ngram_BWA_core.pl <source_name> <query>\n";
}

$source = shift;
$q = shift;
while($_ = shift){
	$q .= " $_";
}
$q =~ s|^\s+||;
$q =~ s|\s+$||;
my $sth = $dbh->prepare("SELECT `id`, `en`, `ch` FROM johntsai.`law` WHERE (ch LIKE ? OR en LIKE ?) AND source LIKE ?");
$sth->execute("%$q%","%$q%",$source);
while(my ($id,$en,$ch)=$sth->fetchrow_array()){
	print STDERR "$id:\n$en\n$ch\n=\n";

	if($en ne "" and $ch ne ""){
		$en =~ s|\r||g;
		$en =~ s|\n||g;
		$ch =~ s|\n||g;
		$ch =~ s|\r||g;
		# push results into @lines array
		my $tmp;
		$tmp->{string1} = $en;	# english string 
		$tmp->{string2} = $ch;	# ch
		push @lines, $tmp;
	}
}

# Call Berkeley Word Aligner
$ret = aligner($q,$source,@lines);
print $ret,"\n";

print STDERR "done.\n";

