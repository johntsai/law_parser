#!/usr/bin/perl -w
# Usage: perl law_align.pl <file_name>
# Transform the raw data copied from pdf file into an aligned file.

use strict;
use encoding "utf8";
use utf8;

my $filename = "";
my $DEBUG;
if(@ARGV>1){
	$filename = shift;
	my $check=shift;
	$DEBUG=1 if($check >0);
}elsif(@ARGV>0){
	$filename = shift;
	$DEBUG = 0;
}else{
	die "Usage: perl law_align.pl <file_name> [enable_debug_mode]";
}

if($DEBUG>0){
	print STDERR "DEBUG Mode ON\n";
}else{
	print STDERR "DEBUG Mode OFF\n";
}
my $outname = $filename.".out";

my $ch_text = "";
my $en_text = "";
my ($ch,$en);
my (@ch_lines,@en_lines);
my (@ch_ids,@en_ids);
my ($ch_id,$en_id);
my $en_count = 0;
my $ch_count = 0;
my $break_flag = 0;
my $break_flag_ch = 0;
open(FD,"<$filename");
open(OUTPUT,">$outname");
while(my $line = <FD>){
	utf8::decode $line;
	print "+$line\n";
	$line =~ s|\n||g;
	$line =~ s|\r||g;
	$line =~ s|（|(|g;
	$line =~ s|）|)|g;

	if($line =~ m|[a-zA-Z]|){
		#$en_text =~ s|(^Article [0-9][0-9\- ]*)\(.*?\)$|$1|;
		#print "=== $en_text\n";
		#if($en_text =~ s|Article ([0-9][0-9\- ]*)(.*?)(Article [0-9][0-9\- ]*)$|$3|){
		# Remove all things in brackets
		unless($line =~ s|\(.*$||){
			$line =~ s|^.*?\)||;
		}
		#print "After removing brackets:\t$line\n";
		# Break line detection
		$break_flag = 0;
		if($line =~ m|^Article [0-9][0-9\- ]*([^\.0-9\- ]*)|){
			#print "#Head found\n";
			$break_flag = 1;
			my $others = $1;
			if($others ne ""){
				$break_flag = 0;
				print "O: $others\n";
			}
		}
		if($break_flag){
			if($en_text =~ m|^Article ([0-9][0-9\- ]*)(.*)$|){
				$en_id = $1;
				$en = $2;
				$en_id =~ s|\s||g;
				$en =~ s|^([^a-zA-Z]+)||g;
				#print "drop head: $1\n";
				#print "#Is Break\n";
				#print "$en_id:\t$en\n";
				push @en_lines, $en;
				push @en_ids, $en_id;
				$en_count += 1;
				#$en_lines{$en_id}=$en;
				#print "en_lines\{$en_id\}=$en\n";
				#my $trash=<STDIN>;
			}
			$en_text = $line;
		}else{
			$en_text = "$en_text $line";
			#print "EN Buffer:\t$en_text\n";
		}
	}else{
		$line =~ s|^(第[0-9][0-9\- ]*?\s+條)\(.*$|$1|;
		$line =~ s|^.*\)||g;
		#print "<-- $line\n";
		$break_flag_ch = 0;
		if($line =~ m|^第[0-9][0-9\- ]*?\s+條(.*)|){
			#	print "#Head found\n";
			$break_flag_ch = 1;
			my $others = $1;
			$others =~ s|\s+||g;
			if($others ne ""){
				$break_flag_ch = 0;
				print "O: $others\n";
			}
		}
		if($break_flag_ch){
			if($ch_text =~ m|^第([0-9][0-9\- ]*?)\s+條(.*?)$|){
				$ch_id = $1;
				$ch = $2;
				$ch =~ s|^\W*||;
				$ch_id =~ s|\s||g;
				#print "---> $ch\n";
				# Skip the duplicated ones
				if($ch_id eq $ch_ids[$#ch_ids]){
					next;
				}else{
					push @ch_lines, $ch;
					push @ch_ids, $ch_id;
					$ch_count += 1;
					#print "ch_lines\{$ch_id\}=$ch\n";
					#my $trash=<STDIN>;
				}
			}
			$ch_text = "$line\.";
		}else{
			$ch_text = "$ch_text$line"; 
			#print "-Buffer: $ch_text\n";
		}
=head
		print "$ch_lines[0]\n$#ch_lines\n";
		print "$ch_count, $break_flag_ch\n\n";
		chomp(my $trash = <STDIN>);
=cut
	}
}
# Last lines
if($en_text =~ m|^Article ([0-9][0-9\- ]*)\W*(\w.*)$|){
	$en_id = $1;
	$en = $2;
	$en_id =~ s|\s||g;
	#print "#Is Break\n";
	#print "$en_id:\t$en\n";
	push @en_lines, $en;
	push @en_ids, $en_id;
	$en_count += 1;
	#$en_lines{$en_id}=$en;
	#print "en_lines\{$en_id\}=$en\n";
	#my $trash=<STDIN>;
}
if($ch_text =~ m|第([0-9][0-9\- ]*)\s+條(.*?)|){
	$ch_id = $1;
	$ch = $2;
	$ch_id =~ s|\s||g;
	#print "---> $ch\n";
	push @ch_lines, $ch;
	push @ch_ids, $ch_id;
	$ch_count += 1;
	#print "ch_lines\{$ch_id\}=$ch\n";
	#my $trash=<STDIN>;
}
print "\n";
#my $trash=<STDIN>;
my $i;
my $die_flag = 0;
#for $i(0..$#ch_lines){
for $i(0..$#en_lines){
	print OUTPUT "=$en_ids[$i]\n=$ch_ids[$i]\n" if($DEBUG);
	print OUTPUT "$en_lines[$i]\n$ch_lines[$i]\n";
	if($en_ids[$i] ne $ch_ids[$i]){
		print STDERR "===BREAK!!!===\n";
		print OUTPUT "=FAIL=\n";
		$die_flag = 1;
		last;
	}
}
if($en_count>$ch_count and $die_flag==0){
	unless($en_ids[$i] ne $ch_ids[$i]){
		for $i($i..$#en_lines){
			print OUTPUT "$en_lines[$i]\n$ch_lines[$i]\n";
		}
	}
}
print "en_lines: $en_count\n";
print "ch_lines: $ch_count\n";
close(FD);
close(OUTPUT);
print STDERR "END Parsing $filename with DEBUG=$DEBUG\n";
